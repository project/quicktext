
Drupal.behaviors.quicktext = function() {
  $.fn.quicktext.init();
};

$.fn.quicktext = {

  init: function(options) {
    var $settings = Drupal.settings.quicktext;
    if ($($settings).length <= 0) return false;

    var settings = {
      'show': $settings.quicktext_show,
      'buttons': $settings.quicktext_buttons,
      'formats': $settings.quicktext_input_formats
    };

    if (options) { $.extend(settings, options); }

    // Will do input formats crosschecking here later on...
    switch (settings.show) {
      case 1:
        var $textareas = $('#node-form .form-item textarea');
        break;
      case 2:
        var $textareas = $('#comment-form .form-item textarea');
        break;
      default:
        var $textareas = $('.form-item textarea');
        break;
    }

    if ($textareas.length == 0) return false;

    $textareas.each( function() {
      var $textarea = $(this);
      var $buttons = $('<div class="quicktext_container"></div>');
      var $clear = $('<div class="quicktext_clear">&nbsp;</div>'); // My clear forever until...

      $textarea.before($buttons);
      $.each(settings.buttons, function(i, button) {
        var $button = $('<div class="quicktext_button"></div>');
        var $btn_img = $('<img class="' + i + '"></img>');
        $btn_img.attr({
          'src': Drupal.settings.basePath + button.src,
          'title': button.title
        });
        $button.css('cursor', 'pointer').click( function(e) {
          e.preventDefault();
          var content = $textarea.attr('value');
          var len = content.length;
          var start = $textarea.get(0).selectionStart;
          var end = $textarea.get(0).selectionEnd;
          var sel = content.substring(start, end);

          if (button.suffix) {
            var replace = button.prefix + sel + button.suffix;
          }
          else if (button.prefix) {
            var replace = button.prefix + sel;
          } else if (i == 'quicktext_view') {
            var $current_preview = $('.quicktext_preview');

            if ($current_preview.length == 0) {
              var $preview = $('<div class="quicktext_preview"></div>');
              var $content = $('<div class="content"></div>');

              $preview.append($content);
              $buttons.append($preview);

              var preview_width = $buttons.width();
              var preview_height = $textarea.innerHeight() + 5;
              
              $preview.animate({
                width: preview_width,
                height: preview_height,
                opacity: 1          
              }, 'normal');

              var pb = new Drupal.progressBar('quicktext-preview-progress');
              var $pb = $(pb.element);
              $preview.append($pb).fadeIn('slow');
              var post_content = { 'content': content };
              $.post(Drupal.settings.basePath + 'quicktext_preview', post_content, function(content_preview) {
                $content.append(content_preview);
                $pb.fadeOut('fast', function() {
                  $(this).remove();
                });
              });
            }
            else {
              $current_preview.animate({
                width: 0,
                height: 0,
                opacity: 0          
              }, 'normal', 'linear', function() {
                $(this).remove();
              });
            }
          }
          if (button.prefix || button.suffix) {
            $textarea.attr('value', content.substring(0, start) + replace + content.substring(end, len));
          }
        });
        if (button.class) {
          $button.addClass(button.class);
        }
        $button.append($btn_img);
        $buttons.append($button);
      });
      $buttons.append($clear);
    });

    return $textareas;
  }
};
